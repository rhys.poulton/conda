# The IGWN Conda Distribution

This project defines the IGWN Conda Distribution.
See

<https://computing.docs.ligo.org/conda/>

for all documentation.
