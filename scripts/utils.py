#!/usr/bin/env python3

import jinja2
import logging

from conda_build.config import Config
from conda_build.metadata import (ns_cfg, select_lines, yamlize)

# -- logging ------------------------------------

LOGGER_FORMAT = "[%(asctime)-15s] %(name)s | %(levelname)+8s | %(message)s"
LOGGER_DATEFMT = "%Y-%m-%d %H:%M:%S"

try:
    from coloredlogs import (
        ColoredFormatter,
        install as install_coloredlogs,
    )
except ImportError:
    ColoredFormatter = logging.Formatter
else:
    install_coloredlogs(
        fmt=LOGGER_FORMAT,
        datefmt=LOGGER_DATEFMT,
    )


class Logger(logging.Logger):
    """`~logging.Logger` with a nice format
    """
    def __init__(self, name, level=logging.DEBUG):
        super().__init__(name, level=level)
        colorformatter = ColoredFormatter(
            fmt=LOGGER_FORMAT,
            datefmt=LOGGER_DATEFMT,
        )
        console = logging.StreamHandler()
        console.setFormatter(colorformatter)
        self.addHandler(console)


def create_logger(name, level=logging.INFO):
    """Configure a logger
    """
    return Logger(f"igwn-conda | {name}", level=level)


# -- YAML parsing -------------------------------

# YAML jinja2 variables
CONDA_BUILD_CONFIG = Config()
DEFAULT_YAML_CONFIG = ns_cfg(CONDA_BUILD_CONFIG)


def parse_yaml(data, **config):
    """Read a YAML file from a path
    """
    namespace = DEFAULT_YAML_CONFIG.copy()
    namespace.update(config)
    # render the YAML file using jinja2 to resolve internal variable usage
    data = jinja2.Template(data).render(**namespace)
    # evaluate selectors and return
    return yamlize(select_lines(data, namespace, False))


def igwn_python_versions(config_file):
    for pyv in parse_yaml(open(config_file).read())['python']:
        yield '{0[0]}.{0[1]}'.format(pyv.split('.'))
