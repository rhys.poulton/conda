---
title: Advanced usage
---

# Advanced usage tips and tricks

## Customising Conda behaviour

The behaviour of `conda` can be controlled by adding/removing options in
the conda configuration file, `.condarc`.o

For details on how to customise conda behaviour, see

<https://docs.conda.io/projects/conda/en/latest/user-guide/configuration/use-condarc.html>

## Managing environments

### Cloning an environment {: #clone }

The pre-built environments in CVMFS are read-only, and so cannot be modified
by users.
You can clone an environment as follows:

```shell
conda create --name <target> --clone <source>
```

e.g.:

```shell
conda create --name myigwn-py38 --clone igwn-py38
```

This should create an exact copy of an existing environment in your local directory
(normally `~/.conda`) that you can modify and upgrade freely.

### Using `mamba` to update packages {: #mamba }

[Mamba](https://mamba.readthedocs.io/) is an alternative implementation
of the conda environment solver (the thing that works out which packages
to install) that may exhibit improved performance in some cases.

`mamba` is included in the `base` environment of the IGWN Conda Distribution
in CVMFS, and is included in the
[Mambaforge](https://github.com/conda-forge/miniforge#mambaforge)
distribution recommended for local users.

If you are having issues with `conda install` or similar commands taking
forever to return, try using `mamba install` instead.

!!! info "Read more on conda performance"
    You can read more about conda performance at

    <https://docs.conda.io/projects/conda/en/latest/user-guide/concepts/conda-performance.html>

## Using Conda environments

### Using Conda environments with HTCondor {: #htcondor }

The IGWN Conda Distributions from OASIS can be used easily in HTCondor
workflows; for best results, follow these simple rules in your
HTCondor submit files:

1. use the absolute path of any executables

2. set `transfer_executable = False`

!!! example "Options required to use IGWN Conda in an HTCondor workflow"

    ```ini
    executable = /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py38/bin/python
    transfer_executable = False
    ```

!!! tip "No environment settings necessary"
    If you supply the full path of the `executable` in your HTCondor submit
    file, you should **not** need to `activate` any conda environments before
    submitting the job/workflow, or pass `getenv=True` to your submit file.
    If you need environment settings for other reasons, you should use the
    `environment` command in your submit file, see
    [here](https://htcondor.readthedocs.io/en/stable/users-manual/submitting-a-job.html#environment-variables) for more details.

### Using graphics libraries on Linux {: #graphics }

Low-latency graphics libraries (e.g. implementations of OpenGL) are not
distributed as conda packages on Linux, and as such these are not included
in the IGWN Conda Distribution environments.

!!! warning "Install graphics libraries separately"

    If your workflow requires rendering graphics, you are expected to
    install these low-level libraries yourself, or ask your friendly
    systems adminstrator to do that for you.

With most systems this just requires installing `libGL.so`, but more
graphics-intensive workflows will likely require other libraries.

!!! info "macOS comes batteries-included"

    macOS (at least macOS X) seems to come with the relevant graphics
    libraries pre-loaded.

### Using LaTeX

The IGWN Conda Distribution does not provide (La)TeX, and in general there
is not a usable TeX distribution available from conda-forge or Anaconda.

If you wish to use LaTeX alongside a Conda environment, you should install
TeX using your system package manager, see below for details for
some common operating systems:

!!! warning "Should be good enough for matplotlib"

    The following examples should get you far enough to use TeX with the
    Matplotlib Python library. If you find that other packages are needed,
    please [open a Bug
    ticket](https://git.ligo.org/computing/conda/issues/new?issuable_template=Bug).

#### Macports

```shell
port install \
    texlive \
    dvipng
```

#### Debian/Ubuntu

```shell
apt-get install \
    dvipng \
    texlive-latex-base \
    texlive-latex-extra
```

#### RHEL/Centos/Fedora

```shell
yum install \
    texlive-dvipng-bin \
    texlive-latex-bin-bin \
    texlive-type1cm \
    texlive-collection-fontsrecommended
```
