#!/bin/bash
#
# IGWN Conda Distribution tests for pyDARM
#

# name of package
PKG_NAME="pydarm"
# path of test directory
TEST_PATH="test/"
# other paths from tarball needed by tests
EXTRA_PATHS="*/example_model_files/"

# download and unpackage the tarball
VERSION=$(python -c "
import ${PKG_NAME};
print(${PKG_NAME}.__version__.split(':', 1)[0])
")
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME}/${PKG_NAME}-${VERSION}.tar.gz"
curl -L ${URL} | \
tar -x -z -f - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
  "*/${TEST_PATH}" \
  "${EXTRA_PATHS}" \
|| {
  echo "download failed, skipping...";
  exit 77;
}

python -m pytest -ra --cache-clear --no-header ${TEST_PATH}
