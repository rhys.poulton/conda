#!/bin/bash
#
# IGWN Conda Distribution tests for nessai
#

# download the original tarball which includes the tests/
VERSION=$(python -c "import nessai; print(nessai.__version__)")
URL="https://pypi.io/packages/source/n/nessai/nessai-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/tests" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
python -m pytest \
	-ra \
	-v \
	--cache-clear \
	--no-header \
	-k 'not corner' \
	tests/
