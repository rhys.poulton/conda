#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Calibration
#

python -c "
import gstlal.calibhandler
import gstlal.calibration_parts
"

gstlal_calibration_aggregator --help
gstlal_compute_strain --help
