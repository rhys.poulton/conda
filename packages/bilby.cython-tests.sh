#!/bin/bash
#
# IGWN Conda Distribution tests for bilby.cython
#

# download the original tarball which includes the test/
VERSION=$(python -c "import bilby_cython; print(bilby_cython.__version__)")
URL="https://pypi.io/packages/source/b/bilby.cython/bilby.cython-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" || {
	echo "download failed, skipping...";
	exit 77;
}

# run the test suite
python -m pytest \
	-ra \
	-v \
	--cache-clear \
	--no-header \
	test/
