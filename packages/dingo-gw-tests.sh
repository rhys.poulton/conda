#!/bin/bash
#
# IGWN Conda Distribution tests for dingo-gw
#

# config for this package
PKG_NAME="dingo-gw"
PKG_IMPORT_NAME="dingo"
TESTS_DIR="tests"

# download the original tarball which includes the tests
VERSION=$(python -c "import ${PKG_IMPORT_NAME}; print(${PKG_IMPORT_NAME}.__version__)")
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME}/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/${TESTS_DIR}" || {
	echo "download failed, skipping...";
	exit 77;
}

# run pytest on the tests dir
python -m pytest \
	-ra \
	-v \
	--cache-clear \
	--no-header \
	-k 'not EOB and not FD_f_max_failure and not test_generate_hplus_hcross_m_IMRPhenomXPHM and not test_FD_time_translation_torch' \
	${TESTS_DIR}/
