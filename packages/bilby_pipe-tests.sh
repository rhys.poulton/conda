#!/bin/bash
#
# IGWN Conda Distribution tests for bilby_pipe
#

# download the gitlab tag tarball which includes tests/
PKG_NAME="bilby_pipe"
VERSION=$(python -c "import ${PKG_NAME}; print(${PKG_NAME}.__version__.split(':', 1)[0])")
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME}/${PKG_NAME}-${VERSION}.tar.gz"
curl -L ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") \
	"*/tests" \
	"*/examples" \
|| {
	echo "download failed, skipping...";
	exit 77;
}

python -m pytest -ra --cache-clear --no-header tests/
