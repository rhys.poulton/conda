#!/bin/bash
#
# IGWN Conda Distribution tests for gwpopulation
#

# config for this package
PKG_NAME="gwpopulation"
PKG_IMPORT_NAME="${PKG_NAME/-/_}"
TESTS_DIR="test"

# download the original tarball which includes the tests
VERSION=$(python -c "import ${PKG_IMPORT_NAME}; print(${PKG_IMPORT_NAME}.__version__)")
URL="https://pypi.io/packages/source/${PKG_NAME::1}/${PKG_NAME}/${PKG_NAME}-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/${TESTS_DIR}" "*/priors" || {
	echo "download failed, skipping...";
	exit 77;
}

# run pytest on the tests dir
python -m pytest \
	-ra \
	-v \
	--cache-clear \
	--no-header \
	${TESTS_DIR}/
