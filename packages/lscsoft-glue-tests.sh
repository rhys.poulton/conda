#!/bin/bash
#
# IGWN Conda Distribution tests for LSCSoft GLUE
#

# download the original tarball which includes the tests/
VERSION=$(python -c "import glue; print(glue.__version__)")
URL="https://pypi.io/packages/source/l/lscsoft-glue/lscsoft-glue-${VERSION}.tar.gz"
curl -Ls ${URL} | tar -xzf - --strip-components=1 $(test $(uname) = "Linux" && echo "--wildcards") "*/test" || {
	echo "download failed, skipping...";
	exit 77;
}

# skip lal_verify (it's slow and people shouldn't using glue.lal.LIGOTimeGPS any more)
TEST_OPTIONS="-o lal_verify"

# glue.ligolw tests start failing on Python 3.10
PYTHON_VERSION_GE310=$(python -c "
import sys;
print(str(sys.version_info[:2] > (3, 9)).lower())
")
if ${PYTHON_VERSION_GE310}; then
	TEST_OPTIONS="${TEST_OPTIONS}
-o glue_ligolw_ilwd_verify
-o test_ligolw_ligolw
";
fi

# run the test suite
make -C test VERBOSE=1 V=1 ${TEST_OPTIONS}
