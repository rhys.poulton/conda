#!/bin/bash
#
# IGWN Conda Distribution tests for MetaIO
#

# use GWpy's example XL file
EXAMPLE_XML="$(python -c "
from pathlib import Path
import gwpy.testing
print(Path(gwpy.testing.__file__).parent)
")/data/X1-GWPY_TEST_SEGMENTS-0-10.xml.gz"

# unzip it (lwtselect can't handle zipped files)
gunzip -c ${EXAMPLE_XML} > example.xml

# test LALMetaIO executables
lwtprint example.xml
lwtscan -t segment example.xml
lwtcut example.xml -t segment_definer "version==1" -o test2.xml
lwtselect example.xml -t segment > copy.xml
lwtdiff example.xml copy.xml -t segment
