#!/bin/bash
#
# IGWN Conda Distribution tests for DQSegDB
#

set -ex

# smoke tests
ligolw_dq_query_dqsegdb --help
ligolw_publish_threaded_dqxml_dqsegdb --help
ligolw_segment_insert_dqsegdb --help
ligolw_segment_query_dqsegdb --help
ligolw_segments_from_cats_dqsegdb --help
