# -*- coding: utf-8 -*-
#
# These tests rely on the fact that the parent environment has a valid
# kerberos keytab
#

import os
import subprocess
import sys
from functools import wraps
from unittest import mock

import pytest

from requests import exceptions

from ciecplib.cookies import load_cookiejar
from ciecplib.kerberos import has_credential
from ciecplib.tool.ecp_cert_info import main as ecp_cert_info
from ciecplib.tool.ecp_curl import main as ecp_curl
from ciecplib.tool.ecp_get_cert import main as ecp_get_cert
from ciecplib.tool.ecp_get_cookie import main as ecp_get_cookie

# files to use when testing CVMFS
CVMFS_BASE_DIR = (
    "/cvmfs/igwn.osgstorage.org/frames/O3/hoft/H1/H-H1_HOFT_C00-12696"
)
CVMFS_FILE = {
    # use different files for each cred, I don't trust caching
    "x509-eec": f"{CVMFS_BASE_DIR}/H-H1_HOFT_C00-1269608448-4096.gwf",
    "x509-proxy": f"{CVMFS_BASE_DIR}/H-H1_HOFT_C00-1269612544-4096.gwf",
}

IDENTITY_PROVIDER = "login.ligo.org"


# -- fixtures -----------------------------------

def pytest_skip_network_error(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (  # exceptions we care about
            exceptions.SSLError,
        ):
            raise
        except exceptions.RequestException as exc:
            if (
                isinstance(exc, exceptions.HTTPError)
                and exc.response.status_code == 401
            ):
                # auth fail
                raise
            pytest.skip(str(exc))

    return wrapper


parametrize_creds = pytest.mark.parametrize(
    "cert_file",
    ("eec_file", "proxy_file"),
    indirect=True,
)


@pytest.fixture(scope="session")
def tgt():
    if not has_credential():
        pytest.skip("no valid kerberos ticket to use")


@pytest_skip_network_error
@pytest.fixture(scope="session")
def eec_file(tmp_path_factory, tgt):
    tmp_path = tmp_path_factory.getbasetemp()
    target = str(tmp_path / "x509-eec")
    ecp_get_cert([
        "--debug",
        "--file", target,
        "--identity-provider", IDENTITY_PROVIDER,
        "--kerberos",
        "--verbose",
    ])
    yield target
    os.remove(target)


@pytest_skip_network_error
@pytest.fixture(scope="session")
def proxy_file(tmp_path_factory, tgt):
    tmp_path = tmp_path_factory.getbasetemp()
    target = str(tmp_path / "x509-proxy")
    ecp_get_cert([
        "--debug",
        "--file", target,
        "--identity-provider", IDENTITY_PROVIDER,
        "--kerberos",
        "--proxy",
        "--verbose",
    ])
    yield target
    os.remove(target)


@mock.patch(  # force scitokens to _not_ work
    "igwn_auth_utils.requests.find_scitoken",
    mock.MagicMock(return_value=None),
)
@mock.patch.dict("os.environ")
@pytest.fixture
def cert_file(request):
    cert_file = request.getfixturevalue(request.param)
    os.environ["X509_USER_PROXY"] = cert_file
    for var in (
        "BEARER_TOKEN",
        "BEARER_TOKEN_FILE",
        "SCITOKEN",
        "SCITOKEN_FILE",
        "X509_USER_CERT",
        "X509_USER_KEY",
    ):
        os.environ.pop(var, None)
    yield cert_file


# -- ciecplib tests -----------------------------

@parametrize_creds
def test_ecp_cert_info(cert_file, capsys):
    import yaml
    ecp_cert_info([
        "--file", cert_file,
    ])
    info = yaml.load(capsys.readouterr().out, Loader=yaml.SafeLoader)
    assert info["path"] == cert_file
    assert info["type"] == {
        "x509-eec": "end entity credential",
        "x509-proxy": "RFC 3820 compliant impersonation proxy",
    }[os.path.basename(cert_file)]


def test_ecp_get_cookie(tgt, tmp_path):
    cookiefile = str(tmp_path / "cookies")
    ecp_get_cookie([
        "--cookiefile", cookiefile,
        "--debug",
        "--identity-provider", IDENTITY_PROVIDER,
        "--kerberos",
        "--verbose",
        "https://ldas-jobs.ligo.caltech.edu/~duncan.macleod/",
    ])
    assert len(load_cookiejar(cookiefile)) >= 3


def test_ecp_curl(tgt, tmp_path, capsys):
    cookiefile = str(tmp_path / "cookies")
    ecp_curl([
        "--cookiefile", cookiefile,
        "--debug",
        "--identity-provider", IDENTITY_PROVIDER,
        "--kerberos",
        "https://ldas-jobs.ligo.caltech.edu/~duncan.macleod/hello.html",
    ])
    assert capsys.readouterr().out.strip() == "HELLO"


def test_ecp_curl_reuse(tgt, tmp_path, capsys):
    """Test that `ecp-curl` properly reuses cookies.
    """
    # run ecp-get-cookie to generate the cookies
    cookiefile = str(tmp_path / "cookies")
    ecp_get_cookie([
        "--cookiefile", cookiefile,
        "--debug",
        "--identity-provider", IDENTITY_PROVIDER,
        "--kerberos",
        "https://ldas-jobs.ligo.caltech.edu/~duncan.macleod/",
    ])
    capsys.readouterr()  # reset capture
    assert len(load_cookiejar(cookiefile)) >= 3

    # run ecp-curl in a way that will fail if the
    # cookies from ecp-get-cookie aren't valid
    ecp_curl([
        "--cookiefile", cookiefile,
        "--debug",
        "--identity-provider", IDENTITY_PROVIDER,
        "https://ldas-jobs.ligo.caltech.edu/~duncan.macleod/hello.html",
    ])

    # assert that the output is good
    assert capsys.readouterr().out.strip() == "HELLO"


# -- service tests ------------------------------

@pytest_skip_network_error
@pytest.mark.parametrize("cert_file", (
    "eec_file",
    # gwdatafind can't handle proxies
    pytest.param("proxy_file", marks=pytest.mark.xfail),
), indirect=True)
def test_gwdatafind_ping(cert_file):
    """Test that we can communicate with datafind.ligo.org with this cert.
    """
    from gwdatafind.ui import ping
    ping("https://datafind.ligo.org")


@pytest_skip_network_error
@pytest.mark.parametrize("cert_file", (
    "eec_file",
    # gracedb can't handle proxies
    pytest.param("proxy_file", marks=pytest.mark.xfail),
), indirect=True)
def test_gracedb_credentials(cert_file):
    """Test that we can execute `gracedb credentials server` with this cert.
    """
    from ligo.gracedb.rest import GraceDb
    client = GraceDb(service_url='https://gracedb-test.ligo.org/api')
    resp = client.get_user_info()
    resp.raise_for_status()
    creds = resp.json()
    assert creds["is_internal_user"]


@pytest_skip_network_error
#@pytest.mark.parametrize(
#    "cert_file",
#    (
#        "eec_file",
#        pytest.param("proxy_file", marks=pytest.mark.xfail),
#    ),
#    indirect=True,
#)
@parametrize_creds
def test_dqsegdb2_query(cert_file):
    """Test that we can communicate with datafind.ligo.org with this cert.
    """
    from dqsegdb2.query import query_versions
    assert 1 in query_versions(
        "H1:DMT-ANALYSIS_READY",
        host="https://segments.ligo.org",
    )


@parametrize_creds
def test_cvmfs_read_proprietary_data(cert_file):
    gwf = CVMFS_FILE[os.path.basename(cert_file)]
    try:
        proc = subprocess.run(
            ["/usr/bin/head", "-c4", str(gwf)],
            capture_output=True,
            check=True,
            text=True,
        )
    except subprocess.CalledProcessError as exc:
        if "permission denied" in exc.stderr.lower():
            raise RuntimeError(
                "failed to authorise CVMFS read",
            ) from exc
        # everything else is a distraction
        pytest.skip(str(exc))

    assert proc.stdout == "IGWD"


# -- run ----------------------------------------

if __name__ == "__main__":
    args = sys.argv[1:] or ["-ra", "-v"]
    sys.exit(pytest.main(args=[__file__] + args))
