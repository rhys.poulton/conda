#!/bin/bash
#
# IGWN Conda Distribution tests for GstLAL Burst
#

export TMPDIR=${TMPDIR:-/tmp}

python -c "
import gstlal.snax.feature_extractor
import gstlal.snglbursttable
import gstlal.streamburca
"

gst-inspect-1.0 gstlalburst

gstlal_snax_extract --help
gstlal_snax_dag_offline --help
